# Nautilus script to add timestamps to files
Lets Nautilus add timestamps to files.

Announcement blog post [here](https://bitleaf.de/2018/05/01/new-tool-add-timestamp-to-files-enhances-productivity-in-gnome-nautilus/).

![](media/screenshot.png)

These scripts are heavily inspired by [Arpad Horvath](https://askubuntu.com/questions/51654/automatically-insert-date-into-filename). Kudos!

### Features

  - Can add two different timestamps files and folders:
    - the current day (e.g. 2017-01-01) as a timestamp to files and folders.
    - the day on which the file was modified last.
  - Can process multiple files/folders, and also a mixed selection of files/folders.


### Install
- Copy both`.py` files to `~/.local/share/nautilus/scripts/` (requires sudo)
- Set them executable with `chmod a+x Add-timestamp-today.py` and `chmod a+x Add-timestamp-last-modified.py`


### Customize date format
You can change the char between the date and the file name, e.g. to underscore instead of the default dash. Just edit this string `'%Y-%m-%d-'` in the *.py* files accordingly.

## Support this project

If you want this project to get better, support me with a few cents:

<a href="https://liberapay.com/MathiasRenner/"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a>

### License

![](https://www.gnu.org/graphics/gplv3-127x51.png)

The project is licensed unter the [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html).

Copyright (C) Mathias Renner

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
